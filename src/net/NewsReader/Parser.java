package net.NewsReader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
/**
 * @author Matthias Grebe
 * @version 1.0a
 */

public class Parser {
    public static String parseContent(String content) {
        String artikel = "";
        Document doc = Jsoup.parse(content);
        //hole den Artikel (funktioniert erstaunlich gut auf allen gängigen Nachrichtens
        Elements articleText = doc.select("body").first().select("p");
/*        Elements absatzelement = articleText.select("p");

        for (int i = 0; i < absatzelement.size(); i++) {
            artikel += absatzelement.get(i).text() + System.lineSeparator();
        }*/

// Jetzt kannst du auf den Textinhalt des Artikels zugreifen:
        artikel += articleText.text();

        return artikel;
    }
}
