package net.NewsReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;
/**
 * @author Matthias Grebe
 * @version 1.0a
 */

public class WebsiteReader {
    private static Proxy PROXY = null;


    public static void setProxy(Proxy PROXY){
        WebsiteReader.PROXY = PROXY;
    }

    public static String ReadContent(String link) {
        URL url = null;
        try {
            url = new URL(link);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        String content = "";
        HttpURLConnection connection = null;
        try {
            connection = PROXY != null ? (HttpURLConnection) url.openConnection(PROXY) : (HttpURLConnection) url.openConnection();
//            connection = (HttpURLConnection) url.openConnection();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    content += line + "\n";
                }
            }
        } catch (IOException e) {
            System.out.println("die Adresse: " + link + " konnte nicht aufgelöst werden");
//            throw new RuntimeException(e);
        }
        return content;
    }
}
