package net.jfx;


import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.mod.Settings;

import java.net.ConnectException;
import java.util.Properties;
/**
 * @author Matthias Grebe
 * @version 1.0a
 */


public class ChatGPTGUISettings{
    @FXML
    private VBox fx_settings_root;
    @FXML
    private TextField fx_settings_hostname;
    @FXML
    private TextField fx_settings_port;
    @FXML
    private TextField fx_settings_username;
    @FXML
    private PasswordField fx_settings_password;
    @FXML
    private TextField fx_settings_apikey;
    @FXML
    private ComboBox<String> fx_comboBox_mode;
    @FXML
    private TitledPane fx_titled_pane_expander;

    private Stage rootStage;
    private Settings settings = new Settings();
    private Scene origScene;

    public void initialize(){
        Properties props = settings.getProperties();
        try {
//        rootStage = (Stage) fx_settings_root.getScene().getWindow();
            fx_settings_username.setText(props.getProperty("proxy.user"));
            fx_settings_password.setText(props.getProperty("proxy.password"));
            fx_settings_apikey.setText(props.getProperty("cgpt.apikey"));
            fx_settings_hostname.setText(props.getProperty("proxy.hostname"));
            fx_settings_port.setText((props.getProperty("proxy.port")));
            fx_comboBox_mode.setValue(settings.getModel());
            fx_comboBox_mode.setItems(FXCollections.observableList(settings.getModels()));

            fx_titled_pane_expander.expandedProperty().addListener((obs, oldExpand, newExpand) -> {
                if (newExpand) {
                    double newHeight = rootStage.getHeight() + fx_titled_pane_expander.getHeight() + 20;
                    rootStage.setHeight(newHeight);
                    rootStage.setScene(rootStage.getScene());
                }
                if (oldExpand) {
                    double newHeight = rootStage.getHeight() - fx_titled_pane_expander.getHeight() + 40;
                    rootStage.setHeight(newHeight);
                    rootStage.setScene(rootStage.getScene());
                }
            });
        } catch (Exception e){
            System.out.println("nothing to see here");
        }
    }

    public void setRootStage(Stage stage) {
        this.rootStage = stage;
        origScene = rootStage.getScene();
    }


    public void onSendSettingsSave(ActionEvent ae) {
        Stage settingsStage = (Stage) ((Node) ae.getSource()).getScene().getWindow();
        int port = 80;
        try {
            port =  Integer.parseInt(fx_settings_port.getText());
        } catch (NumberFormatException e) {

        } catch (RuntimeException e) {
            System.out.println("allgemeiner Fehler");
        }

        settings.writeProperties(fx_settings_apikey.getText(),
                fx_comboBox_mode.getValue(),
                fx_settings_username.getText(),
                fx_settings_password.getText(),
                fx_settings_hostname.getText(),
                port

        );
        settingsStage.close();
    }

    public void onSendSettingsCancel(ActionEvent actionEvent) {
        Stage settingsStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        settingsStage.close();
    }
}

