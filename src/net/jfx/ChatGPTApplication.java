package net.jfx;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

/**
 * @author Matthias Grebe
 * @version 1.0
 */

public class ChatGPTApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
//        FXMLLoader fxmlLoader = new FXMLLoader(ChatGPTApplication.class.getResource("ChatGPT-GUI.fxml"));
        URL url = getClass().getResource("/ChatGPT-GUI.fxml");
        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root, 1200, 800);
        stage.setTitle("ChatGPT");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
