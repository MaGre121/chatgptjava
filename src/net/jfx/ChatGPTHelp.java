package net.jfx;

import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
/**
 * @author Matthias Grebe
 * @version 1.0
 *
 */


public class ChatGPTHelp {
    @FXML
    private BorderPane fxBP;

    public void initialize(){
        fxBP.isResizable();
        fxBP.setCenter(new Text("Author: Matthias Grebe\n" +
                "Version: 1.0\n" +
                "E-Mail: matthias@grebe.hamburg\n" +
                "Source: https://gitlab.com/MaGre121/chatgptjava.git"));
    }
}
