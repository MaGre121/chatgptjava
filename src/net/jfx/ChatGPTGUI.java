package net.jfx;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import net.NewsReader.WebsiteReader;
import net.mod.*;
import net.custom.CustomTextArea;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
/**
 * @author Matthias Grebe
 * @version 1.1
 */

public class ChatGPTGUI {
    //FXML Elements
    @FXML
    private AnchorPane fx_root;
    @FXML
    private VBox fx_subRoot;
    @FXML
    private MenuBar fx_menu;
    @FXML
    private TextField fx_role;
    @FXML
    private Label fx_topicLabel;
    @FXML
    private ScrollPane fx_scrollPane;
    @FXML
    private TextArea insert;
    @FXML
    private TextFlow fx_output;
    @FXML
    private Label fxLabelAIRole;
    @FXML
    private HBox fxHboxButton;


    //Engine Elements
    private Settings settings;
    private ChatGPT cgpt;

    public ChatGPTGUI() {
        this.settings = new Settings();
        this.cgpt = new ChatGPT();
        if (settings.getProxy() != null)
            WebsiteReader.setProxy(settings.getProxy());
    }

    //    doing some layouting here
    public void initialize(){
        fx_subRoot.prefWidthProperty().bind(fx_root.widthProperty().subtract(5));
        fx_subRoot.prefHeightProperty().bind(fx_root.heightProperty().subtract(5));
        fx_menu.prefWidthProperty().bind(fx_subRoot.widthProperty());
        fx_topicLabel.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fx_role.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(fxLabelAIRole.widthProperty()));
        fx_scrollPane.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fx_scrollPane.prefHeightProperty().bind(fx_subRoot.heightProperty().divide(1.8));
        fx_output.prefWidthProperty().bind(fx_scrollPane.widthProperty().subtract(20));
        insert.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));
        fxHboxButton.prefWidthProperty().bind(fx_subRoot.widthProperty().subtract(20));

        fx_role.setFont(new Font("UTF-8", 12));

        fx_role.setText(cgpt.getRole());
    }


    @FXML
    protected void onSendButtonClick() {
        if (settings.written){
            settings.update();
        }
        String linkContent = "";
        String insertedText = insert.getText();
        if (insertedText.contains("https://")){
            linkContent = StripLinkFromText.getLink(insertedText);
            insertedText = insertedText + "\n" + linkContent;
        }

        CustomTextArea inText = new CustomTextArea(insertedText);
        inText.prefWidthProperty().bind(fx_output.widthProperty().subtract(5));
        // asking chatGPT here
        cgpt.askChatGPT(insertedText);
        String outputText = cgpt.getAnswer();
        CustomTextArea outText = new CustomTextArea(outputText);
        outText.prefWidthProperty().bind(fx_output.widthProperty().subtract(5));
        Text USER = new Text("\nUser:\n");
        Text AI = new Text("\nChatGPT:\n");
        USER.setStyle("-fx-font-weight:bold;" +
                "-fx-fill:red;");
        AI.setStyle("-fx-font-weight:bold;" +
                "-fx-fill:green;");

        fx_output.getChildren().addAll(USER, inText, AI, outText);
        insert.clear();
    }

    //    Clear all windows and functions
    public void onClearButtonClick(){
        cgpt.clearHistory();
        insert.clear();
        fx_output.getChildren().clear();
        fx_role.setText(cgpt.getRole());

    }

    public void onSaveButtonClick(){
        Text ROLE = new Text("Saved conversation to:\t" + cgpt.saveConversation());
        ROLE.setStyle("-fx-font-weight:bold;" +
                "-fx-fill:orange;");
        fx_output.getChildren().add(ROLE);
    }
    public void onRoleChange(KeyEvent key){
        ;
        if (key.getCode() == KeyCode.TAB) {
            Text ROLE = new Text("AI Role set to:\t" + fx_role.getText());
            cgpt.clearHistory();
            cgpt.changeRole(fx_role.getText());
            ROLE.setStyle("-fx-font-weight:bold;" +
                    "-fx-fill:orange;");
            fx_output.getChildren().add(ROLE);
        }
    }
    public void onMenuCloseButton() {
        cgpt.exitProgram();
    }

    public void onMenuSettingsButton() throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ChatGPT-Settings.fxml"));
        Parent root = fxmlLoader.load();
//        URL url = getClass().getResource("/ChatGPT-Settings.fxml");
//        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root);
        stage.setTitle("ChatGPT Settings");
        stage.setScene(scene);
        stage.setResizable(true);
        ChatGPTGUISettings controller = fxmlLoader.getController();
        controller.setRootStage(stage);

        stage.showAndWait();
    }

    public void onMenuAboutButton() throws IOException {
        Stage stage = new Stage();
//        FXMLLoader fxmlLoader = new FXMLLoader(ChatGPTApplication.class.getResource("ChatGPT-Help.fxml"));
        URL url = getClass().getResource("/ChatGPT-Help.fxml");
        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root, 500, 250);
        stage.setTitle("ChatGPT About");
        stage.setScene(scene);
        stage.show();
    }
}
