package net.custom;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
/**
 * @author Matthias Grebe
 * @version 1.0
 */


public class CustomTextArea extends TextArea {
    public CustomTextArea() {
        setWrapText(true);
        setEditable(false);
//        setFont(Font.font("Arial Black", 12));

        sceneProperty().addListener((observableNewScene, oldScene, newScene) -> {
            if (newScene != null) {
                applyCss();
                Node text = lookup(".text");

                // 2)
                prefHeightProperty().bind(Bindings.createDoubleBinding(() -> {
                    return getFont().getSize() + text.getBoundsInLocal().getHeight();
                }, text.boundsInLocalProperty()));

                // 1)
                text.boundsInLocalProperty().addListener((observableBoundsAfter, boundsBefore, boundsAfter) -> {
                    Platform.runLater(() -> requestLayout());
                });
            }
        });
    }
    public CustomTextArea(String initialText) {
        super(initialText);
        setWrapText(true);
        setEditable(false);
//        setPrefWidth(600.);

        // Bind the preferred width and height to the text content and font
//        prefWidthProperty().bind(Bindings.createDoubleBinding(
//                () -> new Text(getText()).getLayoutBounds().getWidth() + 10, textProperty()));
        prefHeightProperty().bind(Bindings.createDoubleBinding(
                () -> new Text(getText()).getLayoutBounds().getHeight() + getFont().getSize() + 10,
                textProperty(), fontProperty()));
    }


}
