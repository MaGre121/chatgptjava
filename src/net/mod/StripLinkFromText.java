package net.mod;

import net.NewsReader.Parser;
import net.NewsReader.WebsiteReader;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * @author Matthias Grebe
 * @version 1.0a
 */

public class StripLinkFromText {
    public static String getLink(String text)  {
        //Dieser Regulären Ausdrück soll URLs filtern
        String urlPattern = "(?i)\\b((?:https?://|www\\d?\\.|[a-z\\d]+\\.[a-z]+\\b/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))";

        Pattern pattern = Pattern.compile(urlPattern);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String url = matcher.group(1);

            System.out.println("Gefundener Link: " + url);
            return Parser.parseContent(WebsiteReader.ReadContent(url));
//            return url;
        } else {
            return "";
        }

    }

}
