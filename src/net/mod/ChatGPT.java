package net.mod;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Matthias Grebe
 * @version 1.1
 */


public class ChatGPT {
    private Settings settings;
    private Messages messages;
    private JsonArray answers;
    private String modelsUrl = "https://api.openai.com/v1/models";
    private List<String> searchList = null;

    public ChatGPT() {
        this.settings=new Settings();
        this.messages=new Messages("completion");
        this.answers=new JsonArray(); //falls man noch was mit den Antworten machen will
    }

    public void askChatGPT(String input) {
        this.messages.addMessage("user",input);
        HttpURLConnection con = null;
        try {
            con = settings.getProxy() != null ? (HttpURLConnection) new URL(settings.getURL()).openConnection(settings.getProxy()) : (HttpURLConnection) new URL(settings.getURL()).openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + settings.getApiKey());
            con.setDoOutput(true);
            con.getOutputStream().write(messages.toString().getBytes(StandardCharsets.UTF_8));

        } catch (IOException e){
            e.printStackTrace();
        }

        Gson gson = new Gson();
        JsonObject output = new JsonObject();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) { //Setting Charset to UTF-8 because Java 1.8* needs this :S
            output = gson.fromJson(in.lines().collect(Collectors.joining()), JsonObject.class);
            answers.add(output);
        } catch (Exception e){
            e.printStackTrace();
        }
        try {
            this.messages.addMessage(output.getAsJsonArray("choices").get(0).getAsJsonObject().get("message").getAsJsonObject());
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public String getAnswer() {
        return this.messages.readLastMessage();
    }


    public void clearHistory(){
        this.messages = new Messages("completion");
    }

    public void exitProgram(){
        System.exit(0);
    }

    public String getRole(){
        return this.messages.getRole();
    }

    public void changeRole(String rolle){
        this.messages.changeRole(rolle);
    }

    public String saveConversation(){
        return this.messages.saveConversation();
    }

    public int getUsedTokens(){
        try {
            return answers.get(answers.size()-1).getAsJsonObject().get("usage").getAsJsonObject().get("total_tokens").getAsInt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void searchJsonObject(JsonObject jsonObject, String searchKey) {
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            JsonElement value = entry.getValue();

            if (key.equals(searchKey)) {
//                System.out.println(entry.getValue().getAsString());
                searchList.add(entry.getValue().getAsString());
//                return entry.getValue().getAsString();
            }

            if (value.isJsonObject()) {
                searchJsonObject(value.getAsJsonObject(), searchKey);  // Rekursiver Aufruf für ein weiteres JsonObject
            } else if (value.isJsonArray()) {
                searchJsonArray(value.getAsJsonArray(), searchKey);    // Aufruf einer separaten Methode für ein JsonArray
            }
        }
    }

    private void searchJsonArray(JsonArray jsonArray, String searchKey) {
        for (JsonElement element : jsonArray) {
            if (element.isJsonObject()) {
                searchJsonObject(element.getAsJsonObject(), searchKey);  // Rekursiver Aufruf für ein JsonObject in einem JsonArray
            } else if (element.isJsonArray()) {
                searchJsonArray(element.getAsJsonArray(), searchKey);    // Rekursiver Aufruf für ein verschachteltes JsonArray
            }
        }
    }

    public List<String> getModels(){
        searchList = new ArrayList<>();
        List<String> modelList = new ArrayList<>();
        HttpURLConnection con = null;
        try {
            con = settings.getProxy() != null ? (HttpURLConnection) new URL(modelsUrl).openConnection(settings.getProxy()) : (HttpURLConnection) new URL(modelsUrl).openConnection();
            con.setRequestProperty("Authorization", "Bearer " + settings.getApiKey());
        } catch (IOException e){
            e.printStackTrace();
        }

        Gson gson = new Gson();
        JsonObject output = new JsonObject();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) { //Setting Charset to UTF-8 because Java 1.8* needs this :S
            output = gson.fromJson(in.lines().collect(Collectors.joining()), JsonObject.class);
        } catch (Exception e){
            e.printStackTrace();
        }
        searchJsonObject(output,"root");
        for (String s : searchList) {
            if (s.contains("gpt-"))
                modelList.add(s);
        }

        return modelList;
    }
}
