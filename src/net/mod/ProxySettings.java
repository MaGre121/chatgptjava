package net.mod;

import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
/**
 * @author Matthias Grebe
 * @version 1.0
 */

public class ProxySettings {

    private Proxy proxy;

    public ProxySettings(String hostname, int port, String username, String password) {
        this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port));
        enableProxyWithCredentials();

        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return (new PasswordAuthentication(username, password.toCharArray()));
            }
        });
    }

    public ProxySettings(String hostname, int port) {
        this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port));
    }
    public ProxySettings() {
        this("", 80);
    }

    private void enableProxyWithCredentials(){
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        System.setProperty("jdk.http.auth.proxying.disabledSchemes", "");
    }

    public Proxy getProxy() {
        return this.proxy;
    }
}
