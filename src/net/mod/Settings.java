package net.mod;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.*;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * @author Matthias Grebe
 * @version 1.1
 * creating, saving and reading properties file %username%\.chatgpt\settings.prop providing
 * Proxy Settings (proxy.hostname, proxy.port, proxy.user, proxy.password)
 * ChatGPT Settings (cgpt.model, cgpt.url, cgtp.apikey) (more to come)
 *
 */
public class Settings {
    private final String path = System.getProperty("user.home") + "//.chatgpt/settings.prop";
    private final String defaultURL = "https://api.openai.com/v1/chat/completions";
    private final String defaultModel = "gpt-3.5-turbo";
    private Properties properties = new Properties();
    private String modelsUrl = "https://api.openai.com/v1/models";
    private List<String> searchList = null;

    public boolean written = false;

    public Settings() {
        checkFolder(path);
        try (InputStream input = Files.newInputStream(Paths.get(path))){
            properties.load(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Properties getProperties() {
        return properties;
    }

    public String getProperty(String string){
        return properties.getProperty(string);
    }

    /*
     * building proxy incl credentials
     * proxy without credentials
     * or without proxy
     * can be used directly for connections
     */
    public Proxy getProxy(){
        ProxySettings proxySettings;
        if (properties.getProperty("proxy.user").equals("")) {
            if (properties.getProperty("proxy.hostname").equals("")) {
                return null;
            } else {
                proxySettings = new ProxySettings(properties.getProperty("proxy.hostname"), Integer.parseInt(properties.getProperty("proxy.port")));
            }
        } else {
            proxySettings = new ProxySettings(properties.getProperty("proxy.hostname"), Integer.parseInt(properties.getProperty("proxy.port")), properties.getProperty("proxy.user"), properties.getProperty("proxy.password"));
        }
        return proxySettings.getProxy();
    }
    public String getApiKey(){
        return properties.getProperty("cgpt.apikey");
    }
    public String getModel(){
        return properties.getProperty("cgpt.model");
    }
    public String getURL(){
        return defaultURL;
    }
    public void writeProperties(String apikey, String username, String password, String hostname, int port){
        writeProperties(apikey,defaultModel,username,password,hostname,80);
    }

    public void writeProperties(String apikey,String model, String username, String password, String hostname, int port){

        try (OutputStream out = new FileOutputStream(path)) {
            Properties prop = new Properties();

            prop.setProperty("cgpt.apikey", apikey);
            prop.setProperty("cgpt.model", model);
//            prop.setProperty("cgpt.url", defaultURL);
            prop.setProperty("proxy.user", username);
            prop.setProperty("proxy.password", password);
            prop.setProperty("proxy.hostname", hostname);
            prop.setProperty("proxy.port", String.valueOf(port));

            prop.store(out, null);
            this.properties = prop;
            written = true;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public Settings update(){
        return new Settings();
    }

    private void checkFolder(String folder){
        Path userhome = Paths.get(folder).getParent();
        try  {
            Files.createDirectory(userhome);
            System.out.println("Created properties Folder");
            System.out.println("provide following information to:\t"+userhome+"\n" +
                    "cgpt.apikey=<apiKey_from_open_AI>\n" +
                    "proxy.hostname=<ProxyHostname>\n" +
                    "proxy.port=<ProxyPort>\n" +
                    "proxy.user=<Username>\n" +
                    "proxy.password=<yourpassword>\n" +
                    "cgpt.model=gpt-3.5-turbo\n" +
                    "cgpt.url=https://api.openai.com/v1/chat/completions\n"
            );
            writeProperties("",defaultModel,"","","",80);
        } catch (IOException e) {
            System.out.println("folder already exists, going on without recreating");
        }
    }
    public void searchJsonObject(JsonObject jsonObject, String searchKey) {
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            JsonElement value = entry.getValue();

            if (key.equals(searchKey)) {
//                System.out.println(entry.getValue().getAsString());
                searchList.add(entry.getValue().getAsString());
//                return entry.getValue().getAsString();
            }

            if (value.isJsonObject()) {
                searchJsonObject(value.getAsJsonObject(), searchKey);  // Rekursiver Aufruf für ein weiteres JsonObject
            } else if (value.isJsonArray()) {
                searchJsonArray(value.getAsJsonArray(), searchKey);    // Aufruf einer separaten Methode für ein JsonArray
            }
        }
    }

    private void searchJsonArray(JsonArray jsonArray, String searchKey) {
        for (JsonElement element : jsonArray) {
            if (element.isJsonObject()) {
                searchJsonObject(element.getAsJsonObject(), searchKey);  // Rekursiver Aufruf für ein JsonObject in einem JsonArray
            } else if (element.isJsonArray()) {
                searchJsonArray(element.getAsJsonArray(), searchKey);    // Rekursiver Aufruf für ein verschachteltes JsonArray
            }
        }
    }

    public List<String> getModels() {
        searchList = new ArrayList<>();
        List<String> modelList = new ArrayList<>();
        HttpURLConnection con = null;
        try {
            con = getProxy() != null ? (HttpURLConnection) new URL(modelsUrl).openConnection(getProxy()) : (HttpURLConnection) new URL(modelsUrl).openConnection();
            con.setRequestProperty("Authorization", "Bearer " + getApiKey());
        } catch (IOException e){
            e.printStackTrace();
        }

        Gson gson = new Gson();
        JsonObject output = new JsonObject();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) { //Setting Charset to UTF-8 because Java 1.8* needs this :S
            output = gson.fromJson(in.lines().collect(Collectors.joining()), JsonObject.class);
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("cannot connect to openAi, maybe wrong proxy Settings or no API-key");
        }
        searchJsonObject(output,"root");
        for (String s : searchList) {
            if (s.contains("gpt-"))
                modelList.add(s);
        }

        return modelList;
    }

}
