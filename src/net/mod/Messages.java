package net.mod;

import com.google.gson.*;

import java.io.*;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * @author Matthias Grebe
 * @version 1.0
 */


public class Messages {
    private JsonObject data;
    private JsonArray messagesArray;
    private final String path = Paths.get(System.getProperty("user.home") + "//.chatgpt//dummy.path").getParent().toString();
    private final String messageUsage;

    public Messages() {
        this.messageUsage = "completion";
    }
    public Messages(String messageUsage) {
        this.messageUsage = messageUsage;
//        String file = System.getProperty("user.home") + "//.chatgpt//" + messageUsage + ".json";
            InputStream inputStreat = getClass().getResourceAsStream("/" + this.messageUsage + ".json");
            try (BufferedReader input = new BufferedReader(new InputStreamReader(inputStreat))) {
//        try (FileReader input = new FileReader(file)){
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                this.data = gson.fromJson(input, JsonObject.class);
                this.messagesArray = this.data.getAsJsonArray("messages");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }


    public String readLastMessage(){
        return this.messagesArray.get(this.messagesArray.size()-1).getAsJsonObject().get("content").getAsString();
    }

    public void addMessage(String role, String input){
        JsonObject newMessage = new JsonObject();
        newMessage.addProperty("role", role);
        newMessage.addProperty("content", input);
        this.messagesArray.add(newMessage);
    }

    public void addMessage(JsonObject jsonObject){
        this.messagesArray.add(jsonObject);
    }

    public void changeRole(String input){
        JsonObject newRole = new JsonObject();
        newRole.addProperty("role", "system");
        newRole.addProperty("content", input);
        try {
            this.messagesArray.set(0, newRole);
        } catch (IndexOutOfBoundsException e){
            this.messagesArray.add(newRole);
        }
    }

    public void changemodel(String input){
        JsonPrimitive model = new JsonObject().getAsJsonPrimitive("model");
        model = new JsonPrimitive(input);
        this.data.add("model", model);
    }

    public String getModel(){
        return this.data.get("model").getAsString();
    }

    public String getRole(){
        return this.messagesArray.get(0).getAsJsonObject().get("content").getAsString();
    }

//    public void clearMessages(){
//        this.messagesArray = new JsonArray();
//        this.data.add("messages", this.messagesArray);
//        changeRole("You are a helpful assistant");
//    }

    public String saveConversation(){
        StringBuilder sb = new StringBuilder("<html>\n<body>\n");
        for (JsonElement jsonElement : this.messagesArray) {
            sb.append("<p>\n\t<b style=\"color:red\">").append(jsonElement.getAsJsonObject().get("role").getAsString()).append(":</b><br>\n\t")
                    .append(jsonElement.getAsJsonObject().get("content").getAsString()).append("<br>\n</p>\n");
        }
        sb.append("</body>\n</html>");


        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
        String fileName = date.format(formatter) + ".html";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path+"/history/"+fileName))) {
            writer.write(sb.toString());
        } catch (IOException e) {
            System.err.println("Error writing to file: " + e.getMessage());
        }
        return fileName;
    }

    public JsonElement returnAsJsonElement(){
        return this.data;
    }
    public JsonObject returnAsJsonObject(){return this.data;}

    @Override
    public String toString() {
        return data.toString();
    }
}
